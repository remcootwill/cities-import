package net.twill.rollout;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.WordUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class NetherlandsCitiesRolloutScriptGenerator {

    private final static int COUNTRY_CODE = 17;

    private final static String COUNTRY_NAME = "Belgium";

    private final static String CSO_OWNER_ID = "NLROT01";

    private static final List<String> ports = Arrays.asList(
            "NLROT"
    );

    public static void main(String[] args) throws Exception {

        try (
                InputStream is = Files.newInputStream(
                        Paths.get("/Users/remcoo/Downloads/", "template Benelux.xlsx"),
                        StandardOpenOption.READ
                )
        ) {

            final XSSFWorkbook workbook = new XSSFWorkbook(is);
            final XSSFSheet sheetAt = workbook.getSheetAt(1);

            final Path path = Paths.get("/Users/remcoo/Documents/", "V67_1__TWILL-3988-the-netherlands-cities.sql");

            try (OutputStream os = Files.newOutputStream(path, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE)) {

                try {
                    String prefix = "BEGIN TRY\n" +
                            "  DROP TABLE CityDataForImport;\n" +
                            "END TRY\n" +
                            "BEGIN CATCH\n" +
                            "END CATCH\n" +
                            "\n" +
                            "create table CityDataForImport (\n" +
                            "  cityname varchar(255),\n" +
                            "  citycode varchar(10),\n" +
                            "  portcode varchar(10),\n" +
                            "  distance numeric(10),\n" +
                            "  lookedUpPortUUID varchar(255),\n" +
                            "  resultingCityUUID varchar(255),\n" +
                            "  shouldImport bit\n" +
                            ");\n\n";
                    os.write(prefix.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                sheetAt.iterator().forEachRemaining(row -> {
                    if (row.getRowNum() == 0)
                        return;

                    if (!row.getCell(1).getStringCellValue().equalsIgnoreCase("NL")) {
                        return;
                    }

                    final Cell cell = row.getCell(3);
                    if (cell == null)
                        return;

                    ports.forEach(port -> {
                        try {
                            os.write(String.format(
                                    "INSERT INTO CityDataForImport (cityname,citycode,portcode,distance) VALUES ('%s','%s','%s',1);\n",
                                    concateApetrof(WordUtils.capitalizeFully(cell.getStringCellValue(), new char[]{' ', '('})),
                                    row.getCell(2),
                                    port
                            ).getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                });

                try {
                    String suffix = "update CityDataForImport set lookedUpPortUUID = (select Port.uuid from Port where Port.portUNCode = CityDataForImport.portcode and Port.cso = (select top 1 uuid from cso where cso.ownerid='" + CSO_OWNER_ID + "'));\n" +
                            "update CityDataForImport set shouldImport = (case lookedUpPortUUID when null then 0 else 1 end);\n" +
                            "update CityDataForImport set shouldImport = 0 where shouldImport = 1 and citycode in (select damcoCityCode from city where countryCode = " + COUNTRY_CODE + ");\n" +
                            "\n" +
                            "insert into city (uuid, created, lastModified, countryCode, damcoCityCode, name)\n" +
                            "\tselect lower(newid()), getdate(), getdate(), " + COUNTRY_CODE + " /* country " + COUNTRY_NAME + " */, citycode, cityname\n" +
                            "\tfrom CityDataForImport\n" +
                            "\twhere shouldImport = 1\n" +
                            "\tgroup by citycode, cityname;\n" +
                            "\n" +
                            "update CityDataForImport set resultingCityUUID = (select top 1 uuid from city where city.damcoCityCode = CityDataForImport.citycode and city.countryCode = " + COUNTRY_CODE + " /* country " + COUNTRY_NAME + " */);\n" +
                            "-- Please note that damcoCityCode is only unique within a country\n" +
                            "\n" +
                            "insert into cityportdistance (uuid, created, lastModified, distanceKm, city, port)\n" +
                            "\tselect lower(newid()), getdate(), getdate(), distance, resultingCityUUID, lookedUpPortUUID\n" +
                            "\tfrom CityDataForImport\n" +
                            "\twhere shouldImport = 1;";
                    os.write(suffix.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static String concateApetrof(String value) {
        final String[] split2 = value.split("'");
        if (split2.length > 1) {
            return split2[0] + "''" + split2[1];
        } else {
            return value;
        }
    }
}
