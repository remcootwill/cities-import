package net.twill.rollout;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class USCitiesRolloutScriptGenerator {

	private static final List<String> ports = Arrays.asList(
			"USLSA", "USLGB", "USNYC", "USNWK",
			"USSAV", "USNFK", "USOAK",
			"USTAC", "USHOU", "USCHS",
			"USSEA", "USPEV", "USMIA",
			"USBAL", "USPHL", "USJAX",
			"USILM", "USSJU", "USILG",
			"USBOS", "USNOL"
	);

	public static void main(String[] args) throws Exception {

		try (InputStream is = Files.newInputStream(Paths.get("/Users/remcoo/Downloads/", "us-cities.xlsx"), StandardOpenOption.READ)) {

			final XSSFWorkbook workbook = new XSSFWorkbook(is);
			final XSSFSheet sheetAt = workbook.getSheetAt(1);

			ports.forEach(port -> {

				int index = (ports.indexOf(port) + 2);
				final Path path = Paths.get("/Users/remcoo/Documents/", "V60_" + index + "__TWILL-3824-usa-cities-rollout.sql");

				try (OutputStream os = Files.newOutputStream(path, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE)) {

					try {
						String prefix = "BEGIN TRY\n" +
								"  DROP TABLE CityDataForImport;\n" +
								"END TRY\n" +
								"BEGIN CATCH\n" +
								"END CATCH\n" +
								"\n" +
								"create table CityDataForImport (\n" +
								"  cityname varchar(255),\n" +
								"  citycode varchar(10),\n" +
								"  portcode varchar(10),\n" +
								"  distance numeric(10),\n" +
								"  lookedUpPortUUID varchar(255),\n" +
								"  resultingCityUUID varchar(255),\n" +
								"  shouldImport bit\n" +
								");\n\n";
						os.write(prefix.getBytes());
					} catch (IOException e) {
						e.printStackTrace();
					}

					sheetAt.iterator().forEachRemaining(row -> {
						if (row.getRowNum() == 0)
							return;

						String city = WordUtils.capitalizeFully(row.getCell(3).toString(), new char[]{' ', '\''});
						final Cell cell = row.getCell(4);
						if (cell != null && !StringUtils.isBlank(cell.toString())) {
							city += "," + cell.toString();
						}

						try {
							os.write(String.format(
									"INSERT INTO CityDataForImport (cityname,citycode,portcode,distance) VALUES ('%s','%s','%s',1);\n",
									concateApetrof(city),
									row.getCell(2),
									port
							).getBytes());
						} catch (IOException e) {
							e.printStackTrace();
						}
					});

					try {
						String suffix = "\nupdate CityDataForImport set lookedUpPortUUID = (select Port.uuid from Port where Port.portUNCode = CityDataForImport.portcode and Port.cso = (select top 1 uuid from cso where cso.ownerid='USLAX01')) where lookedUpPortUUID IS NULL;\n" +
								"update CityDataForImport set lookedUpPortUUID = (select Port.uuid from Port where Port.portUNCode = CityDataForImport.portcode and Port.cso = (select top 1 uuid from cso where cso.ownerid='USJFK01')) where lookedUpPortUUID IS NULL;\n" +
								"update CityDataForImport set lookedUpPortUUID = (select Port.uuid from Port where Port.portUNCode = CityDataForImport.portcode and Port.cso = (select top 1 uuid from cso where cso.ownerid='USCNC01')) where lookedUpPortUUID IS NULL;\n" +
								"update CityDataForImport set lookedUpPortUUID = (select Port.uuid from Port where Port.portUNCode = CityDataForImport.portcode and Port.cso = (select top 1 uuid from cso where cso.ownerid='USCHI01')) where lookedUpPortUUID IS NULL;\n" +
								"\n" +
								"update CityDataForImport set shouldImport = (case lookedUpPortUUID when null then 0 else 1 end);\n" +
								"update CityDataForImport set shouldImport = 0 where shouldImport = 1 and citycode in (select damcoCityCode from city where countryCode = 15 /* country United States */);\n" +
								"\n" +
								"insert into city (uuid, created, lastModified, countryCode, damcoCityCode, name)\n" +
								"\tselect lower(newid()), getdate(), getdate(), 15 /* country United States */, citycode, cityname\n" +
								"\tfrom CityDataForImport\n" +
								"\twhere shouldImport = 1\n" +
								"\tgroup by citycode, cityname;\n" +
								"\n" +
								"update CityDataForImport set resultingCityUUID = (select top 1 uuid from city where city.damcoCityCode = CityDataForImport.citycode and city.countryCode=15 /* country United States */);\n" +
								"-- Please note that damcoCityCode is only unique within a country\n" +
								"\n" +
								"insert into cityportdistance (uuid, created, lastModified, distanceKm, city, port)\n" +
								"\tselect lower(newid()), getdate(), getdate(), distance, resultingCityUUID, lookedUpPortUUID\n" +
								"\tfrom CityDataForImport;\n";
						os.write(suffix.getBytes());
					} catch (IOException e) {
						e.printStackTrace();
					}

				} catch (IOException e) {
					e.printStackTrace();
				}
			});
		}



	}

	private static String concateApetrof(String value) {
		final String[] split2 = value.split("'");
		if (split2.length > 1) {
			return split2[0] + "''" + split2[1];
		} else {
			return value;
		}
	}
}
